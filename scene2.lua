-----------------------------------------------------------------------------------------
--
-- scene2.lua
-- 本場景主要示範ShowOverlay這種場景呈現方式，主要用於像是Pause或Menu頁面等場景，Overlay所呈現的場景每次都會重新Create，被蓋的場景則是不會被消滅
-----------------------------------------------------------------------------------------

--=======================================================================================
--引入各種函式庫
--=======================================================================================
local composer = require("composer")
local scene = composer.newScene( )
--require("main")
--=======================================================================================
--宣告全域變數
--=======================================================================================


--=======================================================================================
--宣告區域變數
--=======================================================================================
local img_game
--函式
local initial
local touch
local pause
--=======================================================================================
--定義各種函式
--=======================================================================================

initial = function(group)
	img_game = display.newImageRect( group , "game2.jpg", _SCREEN.WIDTH, _SCREEN.HEIGHT )
	img_game.x = _SCREEN.CENTER.X
	img_game.y = _SCREEN.CENTER.Y
	img_game:addEventListener( "tap", pause )
end

pause = function (  )
	print( "pause" )
	local options = {
    	isModal = true, --事件是否要擋住，以免穿透到被蓋著的場景，預設為false
    	effect = "fade", --overlay效果
    	time = 400,  --動畫時間
    	params = {  --所要傳遞的參數
        	sampleVar = "my sample variable"
    	}
    }
	-- By some method (a pause button, for example), show the overlay
	composer.showOverlay( "pause", options )
	isPause = true
end

--=======================================================================================
--Composer
--=======================================================================================

--畫面沒到螢幕上時，先呼叫scene:create
--任務:負責UI畫面繪製
function scene:create(event)
	print('scene2:create')
	--把場景的view存在sceneGroup這個變數裡
	local sceneGroup = self.view

	--接下來把會出現在畫面的東西，加進sceneGroup裡面
	initial(sceneGroup)
end


--畫面到螢幕上時，呼叫scene:show
--任務:移除前一個場景，播放音效，開始計時，播放各種動畫
function  scene:show( event)
	local sceneGroup = self.view
	local phase = event.phase

	if( "will" == phase ) then
		print('scene2:show will')
		--畫面即將要推上螢幕時要執行的程式碼寫在這邊
	elseif ( "did" == phase ) then
		print('scene2:show did')
		--把畫面已經被推上螢幕後要執行的程式碼寫在這邊
		--可能是移除之前的場景，播放音效，開始計時，播放各種動畫

		--測試Composer Variable
		print('composer variable loading:' .. composer.getVariable('loading').id)
	end
end


--即將被移除，呼叫scene:hide
--任務:停止音樂，釋放音樂記憶體，停止移動的物體等
function scene:hide( event )
	
	local sceneGroup = self.view
	local phase = event.phase

	if ( "will" == phase ) then
		print('scene2:hide will')
		--畫面即將移開螢幕時，要執行的程式碼
		--這邊需要停止音樂，釋放音樂記憶體，有timer的計時器也可以在此停止
	elseif ( "did" == phase ) then
		print('scene2:hide did')
		--畫面已經移開螢幕時，要執行的程式碼
	end
end

--下一個場景畫面推上螢幕後
--任務:摧毀場景
function scene:destroy( event )
	print('scene2:destroy')
	if ("will" == event.phase) then
		--這邊寫下畫面要被消滅前要執行的程式碼

	end
end

--=======================================================================================
--加入偵聽器
--=======================================================================================

scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

return scene