-----------------------------------------------------------------------------------------
--
-- main.lua
-- 本範例示範如何使用Composer架構來製作頁面切換的效果，更多關於Options的設定參考
-- https://docs.coronalabs.com/api/library/composer/gotoScene.html#options	
-- 教學重點:
-- 1.說明Composer重點程式
-- 2.介紹Composer四大金剛(函式)
-- 3.介紹Composer流動順序
-- 4.說明SceneGroup
--
-- Author: Zack Lin
-- Time: 2015/8/19
-----------------------------------------------------------------------------------------

--=======================================================================================
--引入各種函式庫
--=======================================================================================
display.setStatusBar( display.HiddenStatusBar )
--要使用Composer架構必須要先用require指令來匯入Lib
local composer = require( "composer" )
--=======================================================================================
--宣告各種變數
--=======================================================================================
_SCREEN = {
	WIDTH = display.viewableContentWidth,
	HEIGHT = display.viewableContentHeight
}
_SCREEN.CENTER = {
	X = display.contentCenterX,
	Y = display.contentCenterY
}

local str_game = "神魔之塔 V8.0"
local img_loading

--函式
local initial
local onClick
--=======================================================================================
--宣告與定義main()函式
--=======================================================================================
local main = function (  )
	initial()
end

--=======================================================================================
--定義其他函式
--=======================================================================================
initial = function ()
	img_loading = display.newImageRect( "loading.jpg", _SCREEN.WIDTH, _SCREEN.HEIGHT )
	img_loading.id = "img_loading"
	img_loading.x = _SCREEN.CENTER.X
	img_loading.y = _SCREEN.CENTER.Y

	Runtime:addEventListener( "touch", onClick )
end

onClick =  function ( event )
	print('onClick')
	if("ended" == event.phase) then
		--透過設定Variable的機制將變數傳到其他Scene
		composer.setVariable( "game", str_game )
		composer.setVariable( "loading", img_loading )
		
		--將舞台轉移到scene1場景
		local options = { effect = "flip" , time = 2000}
		composer.gotoScene( "scene1" , options)
		Runtime:removeEventListener( "touch", onClick )
	end
end
--=======================================================================================
--呼叫主函式
--=======================================================================================
main()